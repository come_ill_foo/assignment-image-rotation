#include "imageio.h"

const char* img_read_msgs[] = {
  [ READ_OK ] = "image successfully has been read\n",
  [ READ_INVALID_HEADER ] = "encountered invalid format header\n",
  [ READ_INVALID_BITS ] = "encountered invalid bit map\n",
  [ READ_INVALID_SIGNATURE ] = "encountered invalid signature\n",
  [ READ_NOT_SUPPORTED ] = "this format is not supported for reading\n",
  [ READ_NO_FILES ] = "no file provided\n"
};

const char* img_write_msgs[] = {
  [ WRITE_OK ] = "image successfully has been written\n",
  [ WRITE_ERROR ] = "encountered errors while saving image\n",
  [ WRITE_NOT_SUPPORTED ] = "this format is not supported for writing\n",
  [ WRITE_NO_FILES ] = "no file provided\n"
};

static enum read_status from_plug( FILE* const in, struct image* const img ) {
  return READ_NOT_SUPPORTED;
}

static enum write_status to_plug( FILE* const in, const struct image* img ) {
  return WRITE_NOT_SUPPORTED;
}

bool string_ends_with(const char * str, const char * suffix)
{
  size_t str_len = strlen(str);
  size_t suffix_len = strlen(suffix);

  return 
    (str_len >= suffix_len) &&
    (0 == strcmp(str + (str_len-suffix_len), suffix));
}

const struct img_descr formats[] = {
  [ IMG_BMP ] = { ".bmp", from_bmp, to_bmp },
  [ IMG_PNG ] = { ".png", from_plug, to_plug },
  [ IMG_JPG ] = { ".jpg", from_plug, to_plug },
  [ IMG_DEF ] = { ""    , from_plug, to_plug }
};

static bool is_bmp_header_invalid( struct bmp_header head ) {
  // step 0: check if bfType is invalid
  info("+ checking bfType\n");
  const bool is_bfType_invalid = head.bfType != BMP_HEADER_VALID_BFTYPE; // bfType = "BM"
  if ( is_bfType_invalid )
    return is_bfType_invalid;
  info("+ bfType has the valid value\n");

  // step 1: check if bfSize is not equal to sum of biSizeImage and bOffBits
  // should also include check if biSizeImage is not zero otherwise it point to uncompressed RGB bitmaps
  info( "+ checking bfileSize\n" );
  const bool is_bfileSize_invalid = ( head.biSizeImage != 0 ) && head.bfileSize != ( head.biSizeImage + head.bOffBits );
  if ( is_bfileSize_invalid ) 
    return is_bfileSize_invalid;
  info( "+ bfileSize has the valid value\n" );

  // check bfReserved ommitted because as english wikipedia says:
  // its actual value depends on the application that creates the image,
  // if created manually __can__ be 0

  // step 2: check if bOffBits is not equal to size of bmp header structure
  info( "+ checking bOffBits\n" );
  const bool is_bOffBits_invalid = head.bOffBits != sizeof( struct bmp_header );
  if ( is_bOffBits_invalid )
    return is_bOffBits_invalid;
  info( "+ bOffBits has the valid value\n" );
  // ?maybe useless or destructive check

  // step 3: check if biSize is not equal to 40
  info( "+ checking biSize\n" );
  const bool is_biSize_invalid = head.biSize != 40;
  if ( is_biSize_invalid )
    return is_biSize_invalid;
  info( "+ biSize has the valid value\n" );
  // ?maybe useless or destructive check

  // step 4: check if biPlanes is not equal to 1 because of deprecated value
  info( "+ checking biPlanes\n" );
  const bool is_biPlanes_invalid = head.biPlanes != 1;
  if ( is_biPlanes_invalid )
    return is_biPlanes_invalid;
  info( "+ biPlanes has the valid value\n" );

  // step 5: check if bpp is not equal to 24
  info( "+ checking bpp\n" );
  const bool is_bpp_invalid = head.biBitCount != 24;
  if ( is_bpp_invalid )
    return is_bpp_invalid;
  info( "+ bpp has the valid value\n" );

  // step 6: check if biCompression is not equal to uncompressed constant BI_RGB
  info( "+ checking biCompression\n" );
  const bool is_biCompression_invalid = head.biCompression != BI_RGB;
  if ( is_biCompression_invalid )
    return is_biCompression_invalid;
  info( "+ biCompression has the valid value\n" );

  // step 7: check if biSizeImage is not equal to real size of image that can be computed
  // should also include check if biSizeImage is not zero otherwise it point to uncompressed RGB bitmaps
  info( "+ checking biSizeImage\n" );
  const bool is_biSizeImage_invalid = ( head.biSizeImage != 0 ) && head.biSizeImage != ( head.biHeight * ( head.biWidth * sizeof( struct pixel ) + head.biWidth % 4 ) );
  if ( is_biSizeImage_invalid )
    return is_biSizeImage_invalid;
  info( "+ biSizeImage has the valid value\n" );
  // ?maybe useless or destructive check

  // check of biXPelsPerMeter and biYPelsPerMeter ommitted
  // because its value depends on display

  // check of biClrUsed and biClrImportant also ommitted
  // these values also depends on hardware specification

  info( "checking bmp header successfully finished...\n" );
  return false;
}

enum read_status from_bmp( FILE* const in, struct image* const img ) {
  info( "deserialization started\n" );
  // step 0: read file header
  struct bmp_header head = {0};
  info( "read bmp file header\n" );
  size_t numreadheaders = fread( &head, sizeof( struct bmp_header ), 1, in );
  info( "number of read bmp headers: [ %zu ]\n", numreadheaders );

  // step 1: check if file has enough bytes for header 
  if ( numreadheaders < 1 )
    return READ_INVALID_SIGNATURE;
  info( "found at least one bmp header\n" );

  // step 2: check if header has an invalid signature
  info( "checking bmp header started...\n" );
  if ( is_bmp_header_invalid( head ) )
    return READ_INVALID_HEADER;
  info( "found a valid bmp header\n" );

  // step 3: set file pointer to the beginning of the bit map
  info( "try to set file pointer ot the beginning of the bit map\n" );
  if ( fseek( in, head.bOffBits, SEEK_SET ) )
    return READ_INVALID_SIGNATURE;
  info( "file pointer successfully moved\n" );

  // step 4: create image
  info( "file pointer successfully moved\n" );
  *img = image_create( head.biWidth, head.biHeight );

  // step 5: calculating padding
  const int32_t padding = img->width % 4;
  info( "img: [ width: %"PRIu32" ; height: %"PRIu32" ]; padding: [ %"PRId32" ]\n", img->width, img->height, padding );

  // step 6: starting filling img->data with pixels colour values
  info( "filling up the image bit map started...\n" );
  for ( uint32_t i = 0; i < img->height; ++i ) {
    // step 7: read full row of pixels
    info( "reading the [ %"PRIu32" ] row\n", i );
    size_t numreadpixels = fread( img->data + i * img->width, sizeof( struct pixel ), img->width, in );
    info( "read [ %zu ] pixels\n", numreadpixels );

    // step 8: check if less than width byte read
    if ( numreadpixels < img->width ) {
      image_destroy( img );
      info( "read less than image width pixels; finishing reading...\n" );
      return READ_INVALID_BITS;
    }

    // step 9: set file pointer to the beginning of the next row
    if ( fseek( in, padding, SEEK_CUR ) ) {
      image_destroy( img );
      info( "can't set file pointer to the next row; finishing reading...\n" );
      return READ_INVALID_BITS;
    }
  }
  info( "filling up the image bit map finished...\n" );

  info( "deserialization finished\n" );
  return READ_OK;
}

enum read_status from_format( FILE* const in, struct image* const img, const enum img_format format ) {
  return formats[ format ].from( in, img );
}

enum read_status from_name( const char* pathin, struct image* const img ) {

  // step 0: open file for reading input image in binary mode
  info( "opening file...\n" );
  FILE* input = fopen( pathin, "rb" );
  if ( input == NULL )
    err( "errors in opening input file with path: [ %s ]\n", pathin );
  info( "file [ %s ] opened\n", pathin );

  // step 1: detect input file type
  enum img_format in_format = IMG_DEF;
  if ( string_ends_with( pathin, formats[ IMG_BMP ].ext ) )
    in_format = IMG_BMP;
  else if ( string_ends_with( pathin, formats[ IMG_PNG ].ext ) )
    in_format = IMG_PNG;
  else if ( string_ends_with( pathin, formats[ IMG_JPG ].ext ) )
    in_format = IMG_JPG;
  else in_format = IMG_DEF;

  // step 2: deserialize file into internal image
  info( "deserializing file into internal representation\n" );
  enum read_status in_status = from_format( input, img, in_format );
  info( "deserializing completed with status code: [ %zu ]\n", in_status );

  // step 3: close input file
  info( "closing file [ %s ]...\n", pathin );
  if ( fclose( input ) == EOF )
    err( "file [ %s ] wasn't closed successfully\n", pathin );
  info( "file [ %s ] successfully closed\n", pathin );

  return in_status;
}

static struct bmp_header bmp_header_create( uint32_t width, uint32_t height ) {
  const uint32_t biSizeImage = height * ( width * sizeof( struct pixel ) + width % 4 ); // calculated size of image in bytes
  const uint32_t bOffBits = sizeof( struct bmp_header );

  return ( struct bmp_header ) {
    .bfType = BMP_HEADER_VALID_BFTYPE, // valid constant "BM" in big-endian
    .bfileSize = bOffBits + biSizeImage, // calculated file size in bytes
    .bfReserved = 0, // "can be zero if write manually"
    .bOffBits = bOffBits, // the size of bmp header struct in bytes
    .biSize = 40, // constant for supported bmp version format
    .biWidth = width, // just the width of image
    .biHeight = height, // just the height of image
    .biPlanes = 1, // must be 1 for this bmp version because this parameter kept for backward compatibility
    .biBitCount = 24, // standard bpp
    .biCompression = BI_RGB, // use this constant for uncompressed images
    .biSizeImage = biSizeImage, // size of image
    .biClrUsed = 0, // to use full kit of colours
    .biClrImportant = 0 // to use full kit of colours
  };
}

enum write_status to_bmp( FILE* const out, const struct image * const img ) {
  // step -1: create a junk array
  const uint8_t rubbish[] = { 0, 0, 0 };

  // step 0: create a valid bmp header
  info( "creating valid bmp header\n" );
  struct bmp_header head = bmp_header_create( img->width, img->height );


  // step 1: write a valid bmp header
  info( "writing bmp header into file\n" );
  if ( fwrite( &head, sizeof( struct bmp_header ), 1, out ) < 1 )
    return WRITE_ERROR;
  info( "bmp header was successfully written into file\n" );


  // step 2: calculating important values
  int32_t padding = img->width % 4;
  info( "calculated padding: [ %"PRId32" ]\n", padding );


  // step 3: write bit map into file
  info( "writing bit map into file\n" );
  for ( uint32_t i = 0; i < img->height; ++i ) {
    // step 4: write the whole row of pixels
    info( "writing the [ %"PRIu32" ] row\n", i );
    if ( fwrite( img->data + i * img->width, sizeof(struct pixel), img->width, out ) < img->width ) return WRITE_ERROR;
    info( "the whole [ %zu ] row was sucessfully written\n", img->width );

    // step 5: write junk bytes for alignment
    info( "writing rubbish for alignment in the [ %"PRIu32" ] row\n", i );
    if ( fwrite( rubbish, 1, padding, out ) < padding ) return WRITE_ERROR;
    info( "data was successfully aligned to padding [ %"PRId32" ]\n", padding );
  }
  info( "bit map was successfully written into file\n" );
  

  // step 6: return constant of success
  return WRITE_OK;
}

enum write_status to_format( FILE* const out, const struct image * const img, const enum img_format format ) {
  return formats[ format ].to( out, img );
}

enum write_status to_name( const char* pathout, const struct image * const img ) {
  // step 0: open file for writing result image in binary mode
  info( "opening file...\n" );
  FILE* output = fopen( pathout, "wb" );
  if ( output == NULL )
    err( "errors in opening file with path: [ %s ]\n", pathout );
  info( "file [ %s ] opened\n", pathout );

  // step 1: detect output file type
  enum img_format out_format = IMG_DEF;
  if ( string_ends_with( pathout, formats[ IMG_BMP ].ext ) )
    out_format = IMG_BMP;
  else if ( string_ends_with( pathout, formats[ IMG_PNG ].ext ) )
    out_format = IMG_PNG;
  else if ( string_ends_with( pathout, formats[ IMG_JPG ].ext ) )
    out_format = IMG_JPG;
  else out_format = IMG_DEF;

  // step 2: serialize internal image into file
  info( "serializing internal representation into output file\n" );
  enum write_status out_status = to_format( output, img, out_format );
  info( "serializing completed with status code: [ %zu ]\n", out_status );

  // step 3: close output file
  info( "closing file [ %s ]...\n", pathout );
  if ( fclose( output ) == EOF )
    err( "file [ %s ] wasn't closed successfully\n", pathout );
  info( "file [ %s ] successfully closed\n", pathout );

  return out_status;
}