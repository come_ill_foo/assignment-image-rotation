#ifndef _IMAGEIO_H
#define _IMAGEIO_H

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <inttypes.h>

#include "log.h"
#include "image.h"

bool string_ends_with(const char * str, const char * suffix);

#define FOR_BMP_HEADER( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD( t, n ) t n ;


/* define bmp_header for Unix, Linux, *BSD and MacOS systems */
#if defined(unix) || defined(__unix__) || defined(__unix)

struct __attribute__((packed)) bmp_header 
{
  FOR_BMP_HEADER( DECLARE_FIELD )
};

#endif /* unix || __unix__ || __unix */


/* define bmp_header for both windows 32 and 64 */
#ifdef _WIN32

#pragma pack(push, 1)
struct bmp_header 
{
  FOR_BMP_HEADER( DECLARE_FIELD )
};
#pragma pack(pop)

#endif /* _WIN32 */

static const uint16_t BMP_HEADER_VALID_BFTYPE = 0x4d42; // ascii: "BM"

enum bmp_compression {
  BI_RGB = 0x0000,
  BI_RLE8 = 0x0001,
  BI_RLE4 = 0x0002,
  BI_BITFIELDS = 0x0003,
  BI_JPEG = 0x0004,
  BI_PNG = 0x0005,
  BI_CMYK = 0x000B,
  BI_CMYKRLE8 = 0x000C,
  BI_CMYKRLE4 = 0x000D
};

enum img_format {
    IMG_BMP = 0,
    IMG_PNG,
    IMG_JPG,
    IMG_DEF
};

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_NOT_SUPPORTED,
    READ_NO_FILES
    /* коды других ошибок  */
};

/*  deserializers   */
typedef enum read_status deserializer( FILE* const in, struct image* const img );

enum read_status from_bmp( FILE* const in, struct image* const img );

enum read_status from_format( FILE* const in, struct image* const img, const enum img_format format );

enum read_status from_name( const char* inpath, struct image* const img );

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_NOT_SUPPORTED,
    WRITE_NO_FILES
    /* коды других ошибок  */
};

/*  serializers   */
typedef enum write_status serializer( FILE* const out, const struct image * const img );

enum write_status to_bmp( FILE* const out, const struct image * const img );

enum write_status to_format( FILE* const out, const struct image * const img, const enum img_format format );

enum write_status to_name( const char* pathout, const struct image * const img );

struct img_descr {
    const char* ext;
    deserializer* from;
    serializer* to;
};

#endif /* _IMAGEIO_H */