#ifndef _IMAGE_H
#define _IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create(uint64_t width, uint64_t height);

void image_destroy(struct image* img);

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source );

#endif /* _IMAGE_H */