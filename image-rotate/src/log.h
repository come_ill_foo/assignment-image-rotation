#ifndef _LOG_H
#define _LOG_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

_Noreturn void err( const char* msg, ... );

void info( const char* msg, ... );

void usage( const char* program, const char* msg, ... );

#endif /* _LOG_H */