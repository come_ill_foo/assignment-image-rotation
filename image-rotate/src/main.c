#include <stdio.h>

#include "log.h"
#include "image.h"
#include "imageio.h"

extern const char* img_read_msgs[];

extern const char* img_write_msgs[];

int main( int argc, char** argv ) {
  // step 0: start
  info( "image rotation started...\n" );

  // step 1: looking for file path
  info( "looking for file path...\n" );
  if ( argc < 2 ) {
    usage( "rotate", "input_file_path [ output_file_path ]\n" );
    err( "attribute [file path] wasn't provided\n" );
  }
  const char* infilepath = argv[1];
  info( "found file path: [ %s ]\n", infilepath );

  // step 2: read file by path into internal representation
  struct image in_image = {0};
  enum read_status in_status = from_name( infilepath, &in_image );

  // step 3: check read status code
  if ( in_status != READ_OK )
    err( img_read_msgs[ in_status ] );
  info( img_read_msgs[ in_status ] );

  // step 4: apply changes to image
  struct image out_image = rotate( in_image );

  // step 5: clear input image.data
  image_destroy( &in_image );

  // step 6: write internal represantion into file by its path
  const char* outfilepath = ( argc < 3 )? "out.bmp" : argv[2];
  enum write_status out_status = to_name( outfilepath, &out_image );

  // step 7: clear output image.data
  image_destroy( &out_image );

  // step 8: check write status code
  if ( out_status != WRITE_OK )
    err( img_write_msgs[ out_status ] );
  info( img_write_msgs[ out_status ] );

  // step 9: finish
  info( "image rotation finished...\n" );
  return 0;
}