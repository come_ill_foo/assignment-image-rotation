#include "image.h"

struct image image_create(uint64_t width, uint64_t height) {
  return ( struct image ) { 
    .width = width, 
    .height = height, 
    .data = ( struct pixel* ) malloc( sizeof( struct pixel ) * width * height ) 
  };
}

void image_destroy(struct image* img) {
  free(img->data);
}

/* создаёт копию изображения, которая повёрнута на 90 градусов по часовой стрелке */
struct image rotate( struct image const source ) {
  struct image dest = image_create( source.height, source.width );

  const uint64_t width_dest = dest.width;
  const uint64_t height_dest = dest.height;

  for ( uint64_t row_dest = 0, col_src = height_dest - 1; row_dest < height_dest; ++row_dest, --col_src )
    for ( uint64_t col_dest = 0, row_src = 0; col_dest < width_dest; ++col_dest, ++row_src )
      dest.data[ row_dest * width_dest + col_dest ] = source.data[ row_src * height_dest + col_src ];

  return dest;
}