#include "log.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  fprintf(stderr, "err: ");
  vfprintf(stderr, msg, args);
  va_end (args);
  exit(1);
}

void info( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  fprintf(stderr, "info: ");
  vfprintf(stderr, msg, args);
  va_end (args); 
}

void usage( const char* program, const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  fprintf(stderr, "usage: %s ", program);
  vfprintf(stderr, msg, args);
  va_end (args); 
}